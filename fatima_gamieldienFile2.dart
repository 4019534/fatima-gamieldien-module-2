sortArrays(){
  String intro = "MTN Business App of the Year winners for ";
   var arrYears = ['2012', '2013', '2014', '2015', '2016', '2017', '2018', '2019', '2020', '2021'];
  
   var arr2012 = ['FNB Banking for the best iOS consumer App','Health ID for the best iOS enterprise App',
              'FNB Banking for the best Blackberry App','TransUnion Dealer Guide for the best Android enterprise App',
              'FNB Banking for the best Android consumer App','Rapidtargets for the best HTML5 App',
              'Matchy for the best Windows App','Plascon Inspire Me for the most innovative App',
              'PhraZApp for the best Garage developer App']; 
   var arr2013 = ['Best IOS Consumer – DStv','Best IOS Enterprise – .comm Telco Data Visualizer',
                  'Best Blackberry app – PriceCheck Mobile','Best Android app Enterprise – MarkitShare',
                  'Best Android app Consumer – Nedbank App Suite','Best HTML 5 app – SnapScan',
                  'Best Windows app – Kids Aid','Most innovative app – bookly',
                  'Best independent (garage) developer – Gautrain Buddy'];
   var arr2014 = ['Best iOS App (Consumer) - SuperSport',
                  'Best iOS App (Enterprise) - SyncMobile ','Best Android App (Consumer) - My Belongings ',
                  'Best Android App (Enterprise) - LIVE Inspect','Best App use of Microsoft Cloud Services - Vigo',
                  'Best App for the Microsoft Platform - Zapper','Best Garage Developer App - Rea Vaya','Most Innovative App - Wildlife tracker'];
   var arr2015 = ['Most Innovative App: VulaMobile, an application geared towards XYZ',
                  'Best Consumer App: DStv Now, an app that lets DStv Premium customers watch shows, movies and sports on their mobile',
                  'Business App of the Year & Best Enterprise App: WumDrop, an on-demand courier service',
                  'Best Enterprise Development App: CPUT Mobile, an app for students from the Cape Peninsula University of Technology to stay up-to-date with all campus news and happenings',
                  'Best Breakthrough Developer App & People’s choice Award: EskomSePush, an app that gives users push notifications about power outages in South Africa',
                  'Best Wild Card App: M4JAM, an app that enables people to complete micro tasks in exchange for cash'];
   var arr2016 = ['Domestly-Best Consumer App','iKhoha – Best Enterprise App','HearZA – Best Enterprise Development App',
                  'Tuta-me – Best Breakthrough Development App','KaChing – Most Innovative App','Friendly Math Monsters for Kindergarten – Best Mobile Gaming App'];
   var arr2017 = ['Best Consumer Solution: SA’s biggest online retailer, Takealot','Best Financial Solution: Standard Bank’s Shyft',
                  'Best Enterprise Solution: iiDENTIFii, a Cape Town-based technology company','Best Hackathon Solution: SiSa',
                  'Best Health Solution: Guardian Health, whose aim is to make health more accessible','Best Agricultural Solution: Murimi is an Android-based mobile app',
                  'Best Campus Cup Solution: Developed by students, UniWise','Best African Solution: Kenyan app, Kazi App','eBest Women in STEM (Science, Technology, Engineering and Math) Solution: Rekindle Learning',
                  'People’s Choice Award: Internet service provider Afrihost','Most Innovative Solution: Hellopay SoftPOS',
                  'Huawei Category 15:\nFirst Place: Road Save\nSecond Place: Matric Live\nThird Place: Hello Paisa'];
   var arr2018 = ['Best Enterprise Solution: Cowa Bunga', 'Best Incubated Solution: Digemy Knowledge Partner and Besmarter','Most Innovative Solution: Bestee',
                  'Best Gaming Solution: The African Cyber Gaming League App (ACGL)','Best Health Solution: dbTrack',
                  'Best Breakthrough Developer: Bestee','Best South African Solution: Stokfella',
                  'Women in STEM Solution: Difela Hymns','Best Education Solution: Xander English 1-20',
                  'Best Financial Solution: Ctrl','Best Agriculture Solution: Khula','People’s Choice Award: ASI Snakes'];
   var arr2019 = ['Best Financial Solution: Naked','Most Innovative Solution: SI Realities','Best Gaming App: Lost Defence',
                  'Best South African App: Franc','Best Health Solution: Vula Mobile',
                  'Best Educational Solution: Matric Live','Best Women in Science Technology Engineering and Mathematics (Stem) Solution: My Pregnancy Journal and LocTransie',
                  'Best Incubated Solution: LocTransie','Best Agricultural Solution: Hydra','People’s Choice Award: Bottles','Best Consumer App: Over','Best Enterprise App: Digger',
                  'Best Breakthrough Developer: Mo Wash'];
   var arr2020 = ['Best Consumer Solution: EasyEquities','Best Women in STEM (science, technology, engineering, and mathematics): Examsta','Best Enterprise Solution: Checkers Sixty60',
                  'Best Incubated Solution: Technishen','Most Innovative Solution: BirdPro','Best Health Solution: Lexie Hearing','Best Gaming Solution: League of Legends',
                  'Best Agricultural Solution: GreenFingers Mobile','Best Educational Solution: Xitsonga Dictionary','Best Financial Solution: StokFella','Best ‘South African’ App: Bottles',
                  'Best Breakthrough Solution: Matric Live','Best Youth App: Guardian Health', 'Huawei Category: My Pregnancy Journey'];
   var arr2021 = ['Best Enterprise Solution: iiDENTIFii app','Most Innovative Solution: Hellopay SoftPOS','Best Health Solution: Guardian Health Platform',
                  'Best Gaming Solution: Ambani Africa','Best Agricultural Solution: Murimi','Best Educational Solution: Ambani Africa','Best Financial Solution: Shyft','Best Hackathon Solution: Sisa',
                  'Best ‘South African’ App: Ambani Africa','Best Campus Cup Solution: UniWise','Best African Solution: Kazi','Best Consumer Solution: Takealot app','Best Women in STEM: Rekindle Learning app',
                  'Huawei Category 15: Roadsave','People’s Choice Award: Afrihost'];
   var arrOverallWinner = ['Overall Winner for 2012: FNB Banking','Overall Winner for 2013: SnapScan','Overall Winner for 2014: LIVE Inspect','Overall Winner for 2015: Wumdrop','Overall Winner for 2016: Domestly',
                           'Overall Winner for 2017: Shyft','Overall Winner for 2018: Khula','Overall Winner for 2019: Naked Insurance','Overall Winner for 2020: EasyEquities','Overall Winner for 2021: Ambani Africa'];
   var overAllArr = [arr2012,arr2013,arr2014,arr2015,arr2016,arr2017,arr2018,arr2019,arr2020,arr2021];
   int totalApps = 0;
  
  for(var arr in overAllArr){
      totalApps+= arr.length;
      String years = arrYears.elementAt(overAllArr.indexOf(arr));
      print("\n" + intro + years +":\n");
      for (String prop in arr) {
        int numbering = arr.indexOf(prop)+1;
        print(numbering.toString() + ") " + prop);
  }
      print("The total number of winning apps for the year: "+arr.length.toString());
      int numbering2 = arrYears.indexOf(years);
      print(arrOverallWinner.elementAt(numbering2)); 
    
  
  }
  
  print('\nThe total number of winning apps of the MTN Business App of the Year Awards since 2012: ' + totalApps.toString());
  print('The total number of overall winning apps of the MTN Business App of the Year Awards since 2012: ' + arrOverallWinner.length.toString());
}


void main() {
   sortArrays(); 
   var arrApps = ['Overall Winning App of 2017: Shyft', 'Overall Winning App of 2018: Khula'];
  print("\nThe winning apps of year 2017 and 2018 are as follows:\n");
  for(var app in arrApps){
     print(app);
}
  }
  
