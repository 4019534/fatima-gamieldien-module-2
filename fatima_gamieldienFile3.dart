class AppInfo{
  String name;
  String category;
  String developer;
  String year;
  
  
  AppInfo(this.name, this.category, this.developer, this.year);
  String toCaps(){
    String name = this.name.toUpperCase();
    return 'Name: ' + name.toUpperCase() + '\nCategory: '+ category + '\nDeveloper: ' + developer + '\nYear: ' + year + '\n';
    
  }
  
 @override
  String toString() {
    return 'Name: ' + name + '\nCategory: '+ category + '\nDeveloper: ' + developer + '\nYear: ' + year + '\n';
  
}
}

sortInfo(){
  var allApps = ['MTN App of the Year Awards Winning Apps of 2012',
                  AppInfo('FNB Banking','Best iOS Consumer App','FNB Connect','2012'),AppInfo('Health ID', 'Best iOS Enterprise App','Discovery Limited','2012'),AppInfo('FNB Banking', 'Best Blackberry App','FNB Connect','2012'),
                  AppInfo('TransUnion Dealer Guide', 'Best Android Enterprise App', 'Business Connexion','2012'),AppInfo('FNB Banking','Best Android Consumer App','FNB Connect', '2012'),
                  AppInfo('Rapidtargets', 'Best HTML5 App', 'OrangeOne Consulting','2012'),AppInfo('Matchy', 'Best Windows App', 'RogueCode', '2012'),
                  AppInfo('Plascon Inspire Me', 'Most Innovative App', '4i Mobile Applications','2012'),AppInfo('PhraZApp', 'Best Garage developer App', 'Glenn Stein','2012'),
              'MTN App of the Year Awards Winning Apps of 2013',
                  AppInfo('DStv', 'Best IOS Consumer', 'Multichoice Support Services (Pty) Ltd', '2013'),AppInfo('.comm Telco Data Visualizer','Best IOS Enterprise','Deloitte Digital', '2013'),
                  AppInfo('PriceCheck Mobile','Best Blackberry App','PriceCheck (Pty) Ltd', '2013'),AppInfo('MarkitShare','Best Android App Enterprise','MarkitShare', '2013'),
                  AppInfo('Nedbank App Suite','Best Android App Consumer','Nedbank Limited', '2013'),AppInfo('SnapScan','Best HTML 5 App', 'FireID (Pty) Ltd', '2013'),
                  AppInfo('Kids Aid','Best Windows App', 'Business Connexion', '2013'),AppInfo('bookly','Most Innovative App', 'Native VML', '2013'),
                  AppInfo('Gautrain Buddy','Best independent (garage) developer','AfriGIS (Pty) Ltd', '2013'),
              'MTN App of the Year Awards Winning Apps of 2014',
                  AppInfo('SuperSport','Best iOS App (Consumer)','Multichoice Support Services (Pty) Ltd', '2014'),
                  AppInfo('SyncMobile','Best iOS App (Enterprise)', 'Bank Independent','2014'),AppInfo('My Belongings','Best Android App (Consumer)','Adam Bulmer ', '2014'),
                  AppInfo('LIVE Inspect','Best Android App (Enterprise)', 'Lightstone Auto','2014'),AppInfo('Vigo','Best App use of Microsoft Cloud Services', ' ', '2014'),
                  AppInfo('Zapper','Best App for the Microsoft Platform', 'Zap Group','2014'),AppInfo('Rea Vaya','Best Garage Developer App','Griffiths Sibeko Company', '2014'),
                  AppInfo('Wildlife tracker','Most Innovative App','', '2014'),
              'MTN App of the Year Awards Winning Apps of 2015',
                  AppInfo('VulaMobile','Most Innovative App','Mafami Pty Ltd', '2015'),AppInfo('DStv Now','Best Consumer App', 'Multichoice Support Services (Pty) Ltd','2015'),
                  AppInfo('WumDrop','Business App of the Year & Best Enterprise App', 'Benjamin Claassen and Muneeb Samuels','2015'),AppInfo('CPUT Mobile','Best Enterprise Development App','Nalube and Ashley Jones,', '2015'),
                  AppInfo('EskomSePush','Best Breakthrough Developer App & People’s choice Award', 'Herman Maritz and Dan Wells','2015'),AppInfo('M4JAM','Best Wild Card App','M4JAM Pty Ltd', '2015'),
              'MTN App of the Year Awards Winning Apps of 2016',
                  AppInfo('Domestly','Best Consumer App', 'Thatoyoana Marumo and Berno Potgieters','2016'),AppInfo('iKhokha','Best Enterprise App','Emerge Mobile (Pty) Ltd', '2016'),
                  AppInfo('HearZA','Best Enterprise Development App', 'hearX Group','2016'),AppInfo('Tuta-me','Best Breakthrough Development App', 'Silicon Maboneng Jozi (PTY) LTD','2016'),
                  AppInfo('KaChing','Most Innovative App', 'Innovation Lounge','2016'), AppInfo('Friendly Math Monsters for Kindergarten','Best Mobile Gaming App','Smart Gecko Software Development (PTY) LTD', '2016'),
              'MTN App of the Year Awards Winning Apps of 2017',
                  AppInfo('Takealot','Best Consumer Solution', 'Takealot Online (Pty) Ltd','2017'),AppInfo('Shyft','Best Financial Solution','Standard Bank Group', '2017'),
                  AppInfo('iiDENTIFii','Best Enterprise Solution','iiDENTIFii', '2017'),AppInfo('SiSa','Best Hackathon Solution', 'Claudia Mabuza','2017'),
                  AppInfo('Guardian Health','Best Health Solution', 'Guardian Health Platform (Pty) Ltd','2017'),AppInfo('Murimi','Best Agricultural Solution', 'Rubiem Technologies','2017'),
                  AppInfo('UniWise','Best Campus Cup Solution', 'OGO Studio','2017'),AppInfo( 'Kazi App','Best African Solution', '','2017'),AppInfo('Rekindle Learning','Best Women in STEM (Science, Technology, Engineering and Math) Solution','Rekindle Learning', '2017'),
                  AppInfo('Hellopay SoftPOS','Most Innovative Solution', 'Transaction Technologies','2017'),
              'Huawei Category 15 for 2017:',
                  AppInfo(' RoadSave','Huawei Category 15 First Place', 'Dynamus Technologies','2017'),AppInfo('Matric Live','Second Place', 'Lesego Finger','2017'),AppInfo('Hello Paisa','Third Place', 'Hello Group (Pty) Ltd','2017'),
              'MTN App of the Year Awards Winning Apps of 2018',
                  AppInfo('Cowa Bunga','Best Enterprise Solution', 'Cowabunga Team','2018'),AppInfo('Digemy Knowledge Partner and Besmarter','Best Incubated Solution', '','2018'),
                  AppInfo('Bestee','Most Innovative Solution', 'Emile Ferreira','2018'),AppInfo('The African Cyber Gaming League App (ACGL)','Best Gaming Solution','African Cyber Gaming League', '2018'),
                  AppInfo('dbTrack','Best Health Solution', 'hearX Group','2018'),AppInfo('Bestee','Best Breakthrough Developer: ', 'Emile Ferreira','2018'),AppInfo('Stokfella','Best South African Solution','Pluritone','2018'),
                  AppInfo('Difela Hymns','Women in STEM Solution','Frans Hanyane', '2018'),AppInfo('Xander English 1-20','Best Education Solution', 'Xander Educational Apps','2018'),
                  AppInfo('Ctrl','Best Financial Solution','Ctrl Technologies (Pty) Ltd', '2018'),AppInfo('Khula','Best Agriculture Solution', 'Karidas Tshintsholo and Matthew Piper','2018'),
                  AppInfo('ASI Snakes','People’s Choice Award','Johan Marais', '2018'),
              'MTN App of the Year Awards Winning Apps of 2019',
                  AppInfo('Naked','Best Financial Solution', ' Naked Insurance', '2019'),AppInfo('SI Realities','Most Innovative Solution', 'Mbangiso Mabaso','2019'),AppInfo('Lost Defence','Best Gaming App', '','2019'),
                  AppInfo('Franc','Best South African App', 'Franc Group Pty Ltd','2019'),AppInfo('Vula Mobile','Best Health Solution', 'Mafami Pty Ltd','2019'),AppInfo('Matric Live','Best Educational Solution','Lesego Finger', '2019'),
                  AppInfo('My Pregnancy Journal and LocTransie','Best Women in Science Technology Engineering and Mathematics (Stem) Solution', 'My Pregnancy Journey PTY Ltd and Ginini Consulting Apps','2019'),
                  AppInfo('LocTransie','Best Incubated Solution','Ginini Consulting Apps', '2019'),AppInfo('Hydra','Best Agricultural Solution', '','2019'),AppInfo('Bottles','People’s Choice Award','Bottles App (PTY) LTD', '2019'),
                  AppInfo('Over','Best Consumer App', 'GoDaddy Mobile, LLC','2019'),AppInfo('Digger','Best Enterprise App','', '2019'),AppInfo('Mo Wash','Best Breakthrough Developer', 'Runtime Solutions (Pty) Ltd','2019'),
              'MTN App of the Year Awards Winning Apps of 2020',
                  AppInfo('EasyEquities','Best Consumer Solution', 'First World Trader (Pty) Ltd', '2020'),AppInfo('Examsta','Best Women in STEM (science, technology, engineering, and mathematics)','', '2020'),
                  AppInfo('Checkers Sixty60','Best Enterprise Solution', 'Shoprite Checkers (PTY) LTD','2020'),AppInfo('Technishen','Best Incubated Solution', 'Tsogolo Technologies (PTY) Ltd','2020'),
                  AppInfo('BirdPro','Most Innovative Solution', 'High Branching cc','2020'),AppInfo('Lexie Hearing','Best Health Solution', 'hearX Group', '2020'),AppInfo('League of Legends','Best Gaming Solution', '','2020'),
                  AppInfo('GreenFingers Mobile','Best Agricultural Solution','Agro-Serve (Pty) Ltd', '2020'),AppInfo('Xitsonga Dictionary','Best Educational Solution', 'Mukondleteri Dumela','2020'),
                  AppInfo('StokFella','Best Financial Solution', 'Pluritone','2020'),AppInfo('Bottles','Best ‘South African’ App', 'Bottles App (PTY) LTD','2020'),
                  AppInfo('Matric Live','Best Breakthrough Solution', 'Lesego Finger','2020'),AppInfo('Guardian Health','Best Youth App','Guardian Health Platform (Pty) Ltd' ,'2020'),
                  AppInfo('My Pregnancy Journey','Huawei Category', 'My Pregnancy Journey PTY Ltd','2020'),
              'MTN App of the Year Awards Winning Apps of 2021',
                  AppInfo('iiDENTIFii','Best Enterprise Solution', 'iiDENTIFii','2021'),AppInfo('Hellopay SoftPOS','Most Innovative Solution', 'Transaction Technologies','2021'),
                  AppInfo('Guardian Health Platform','Best Health Solution', 'Guardian Health Platform (Pty) Ltd','2021'),AppInfo('Ambani Africa','Best Gaming Solution', 'Mukundi Lambani','2021'),
                  AppInfo('Murimi','Best Agricultural Solution','Rubiem Technologies', '2021'),AppInfo('Ambani Africa','Best Educational Solution', 'Mukundi Lambani','2021'),
                  AppInfo('Shyft','Best Financial Solution', 'Standard Bank Group','2021'),AppInfo('Sisa','Best Hackathon Solution', 'Claudia Mabuza','2021'),
                  AppInfo('Ambani Africa','Best ‘South African’ App', 'Mukundi Lambani','2021'),AppInfo('UniWise','Best Campus Cup Solution', 'OGO Studio','2021'),
                  AppInfo('Kazi','Best African Solution','', '2021'),AppInfo('Takealot','Best Consumer Solution', 'Takealot Online (Pty) Ltd','2021'),
                  AppInfo('Rekindle Learning','Best Women in STEM', 'Rekindle Learning','2021'),AppInfo('RoadSave','Huawei Category 15', 'Dynamus Technologies','2021'),
                  AppInfo('Afrihost','People’s Choice Award', 'Afrihost (Pty) Ltd','2021')];
  
  var capsApps = ['MTN App of the Year Awards Winning Apps of 2012',
                  AppInfo('FNB Banking','Best iOS Consumer App','FNB Connect','2012').toCaps(),AppInfo('Health ID', 'Best iOS Enterprise App','Discovery Limited','2012').toCaps(),AppInfo('FNB Banking', 'Best Blackberry App','FNB Connect','2012').toCaps(),
                  AppInfo('TransUnion Dealer Guide', 'Best Android Enterprise App', 'Business Connexion','2012').toCaps(),AppInfo('FNB Banking','Best Android Consumer App','FNB Connect', '2012').toCaps(),
                  AppInfo('Rapidtargets', 'Best HTML5 App', 'OrangeOne Consulting','2012').toCaps(),AppInfo('Matchy', 'Best Windows App', 'RogueCode', '2012').toCaps(),
                  AppInfo('Plascon Inspire Me', 'Most Innovative App', '4i Mobile Applications','2012').toCaps(),AppInfo('PhraZApp', 'Best Garage developer App', 'Glenn Stein','2012').toCaps(),
              'MTN App of the Year Awards Winning Apps of 2013',
                  AppInfo('DStv', 'Best IOS Consumer', 'Multichoice Support Services (Pty) Ltd', '2013').toCaps(),AppInfo('.comm Telco Data Visualizer','Best IOS Enterprise','Deloitte Digital', '2013').toCaps(),
                  AppInfo('PriceCheck Mobile','Best Blackberry App','PriceCheck (Pty) Ltd', '2013').toCaps(),AppInfo('MarkitShare','Best Android App Enterprise','MarkitShare', '2013').toCaps(),
                  AppInfo('Nedbank App Suite','Best Android App Consumer','Nedbank Limited', '2013').toCaps(),AppInfo('SnapScan','Best HTML 5 App', 'FireID (Pty) Ltd', '2013').toCaps(),
                  AppInfo('Kids Aid','Best Windows App', 'Business Connexion', '2013').toCaps(),AppInfo('bookly','Most Innovative App', 'Native VML', '2013').toCaps(),
                  AppInfo('Gautrain Buddy','Best independent (garage) developer','AfriGIS (Pty) Ltd', '2013').toCaps(),
              'MTN App of the Year Awards Winning Apps of 2014',
                  AppInfo('SuperSport','Best iOS App (Consumer)','Multichoice Support Services (Pty) Ltd', '2014').toCaps(),
                  AppInfo('SyncMobile','Best iOS App (Enterprise)', 'Bank Independent','2014').toCaps(),AppInfo('My Belongings','Best Android App (Consumer)','Adam Bulmer ', '2014').toCaps(),
                  AppInfo('LIVE Inspect','Best Android App (Enterprise)', 'Lightstone Auto','2014').toCaps(),AppInfo('Vigo','Best App use of Microsoft Cloud Services', ' ', '2014').toCaps(),
                  AppInfo('Zapper','Best App for the Microsoft Platform', 'Zap Group','2014').toCaps(),AppInfo('Rea Vaya','Best Garage Developer App','Griffiths Sibeko Company', '2014').toCaps(),
                  AppInfo('Wildlife tracker','Most Innovative App','', '2014').toCaps(),
              'MTN App of the Year Awards Winning Apps of 2015',
                  AppInfo('VulaMobile','Most Innovative App','Mafami Pty Ltd', '2015').toCaps(),AppInfo('DStv Now','Best Consumer App', 'Multichoice Support Services (Pty) Ltd','2015').toCaps(),
                  AppInfo('WumDrop','Business App of the Year & Best Enterprise App', 'Benjamin Claassen and Muneeb Samuels','2015').toCaps(),AppInfo('CPUT Mobile','Best Enterprise Development App','Nalube and Ashley Jones,', '2015').toCaps(),
                  AppInfo('EskomSePush','Best Breakthrough Developer App & People’s choice Award', 'Herman Maritz and Dan Wells','2015').toCaps(),AppInfo('M4JAM','Best Wild Card App','M4JAM Pty Ltd', '2015').toCaps(),
              'MTN App of the Year Awards Winning Apps of 2016',
                  AppInfo('Domestly','Best Consumer App', 'Thatoyoana Marumo and Berno Potgieters','2016').toCaps(),AppInfo('iKhokha','Best Enterprise App','Emerge Mobile (Pty) Ltd', '2016').toCaps(),
                  AppInfo('HearZA','Best Enterprise Development App', 'hearX Group','2016').toCaps(),AppInfo('Tuta-me','Best Breakthrough Development App', 'Silicon Maboneng Jozi (PTY) LTD','2016').toCaps(),
                  AppInfo('KaChing','Most Innovative App', 'Innovation Lounge','2016').toCaps(), AppInfo('Friendly Math Monsters for Kindergarten','Best Mobile Gaming App','Smart Gecko Software Development (PTY) LTD', '2016').toCaps(),
              'MTN App of the Year Awards Winning Apps of 2017',
                  AppInfo('Takealot','Best Consumer Solution', 'Takealot Online (Pty) Ltd','2017').toCaps(),AppInfo('Shyft','Best Financial Solution','Standard Bank Group', '2017').toCaps(),
                  AppInfo('iiDENTIFii','Best Enterprise Solution','iiDENTIFii', '2017').toCaps(),AppInfo('SiSa','Best Hackathon Solution', 'Claudia Mabuza','2017').toCaps(),
                  AppInfo('Guardian Health','Best Health Solution', 'Guardian Health Platform (Pty) Ltd','2017').toCaps(),AppInfo('Murimi','Best Agricultural Solution', 'Rubiem Technologies','2017').toCaps(),
                  AppInfo('UniWise','Best Campus Cup Solution', 'OGO Studio','2017').toCaps(),AppInfo( 'Kazi App','Best African Solution', '','2017').toCaps(),AppInfo('Rekindle Learning','Best Women in STEM (Science, Technology, Engineering and Math) Solution','Rekindle Learning', '2017').toCaps(),
                  AppInfo('Hellopay SoftPOS','Most Innovative Solution', 'Transaction Technologies','2017').toCaps(),
              'Huawei Category 15 for 2017:',
                  AppInfo(' RoadSave','Huawei Category 15 First Place', 'Dynamus Technologies','2017').toCaps(),AppInfo('Matric Live','Second Place', 'Lesego Finger','2017').toCaps(),AppInfo('Hello Paisa','Third Place', 'Hello Group (Pty) Ltd','2017').toCaps(),
              'MTN App of the Year Awards Winning Apps of 2018',
                  AppInfo('Cowa Bunga','Best Enterprise Solution', 'Cowabunga Team','2018').toCaps(),AppInfo('Digemy Knowledge Partner and Besmarter','Best Incubated Solution', '','2018').toCaps(),
                  AppInfo('Bestee','Most Innovative Solution', 'Emile Ferreira','2018').toCaps(),AppInfo('The African Cyber Gaming League App (ACGL)','Best Gaming Solution','African Cyber Gaming League', '2018').toCaps(),
                  AppInfo('dbTrack','Best Health Solution', 'hearX Group','2018').toCaps(),AppInfo('Bestee','Best Breakthrough Developer: ', 'Emile Ferreira','2018').toCaps(),AppInfo('Stokfella','Best South African Solution','Pluritone','2018').toCaps(),
                  AppInfo('Difela Hymns','Women in STEM Solution','Frans Hanyane', '2018').toCaps(),AppInfo('Xander English 1-20','Best Education Solution', 'Xander Educational Apps','2018').toCaps(),
                  AppInfo('Ctrl','Best Financial Solution','Ctrl Technologies (Pty) Ltd', '2018').toCaps(),AppInfo('Khula','Best Agriculture Solution', 'Karidas Tshintsholo and Matthew Piper','2018').toCaps(),
                  AppInfo('ASI Snakes','People’s Choice Award','Johan Marais', '2018').toCaps(),
              'MTN App of the Year Awards Winning Apps of 2019',
                  AppInfo('Naked','Best Financial Solution', ' Naked Insurance', '2019').toCaps(),AppInfo('SI Realities','Most Innovative Solution', 'Mbangiso Mabaso','2019').toCaps(),AppInfo('Lost Defence','Best Gaming App', '','2019').toCaps(),
                  AppInfo('Franc','Best South African App', 'Franc Group Pty Ltd','2019').toCaps(),AppInfo('Vula Mobile','Best Health Solution', 'Mafami Pty Ltd','2019').toCaps(),AppInfo('Matric Live','Best Educational Solution','Lesego Finger', '2019').toCaps(),
                  AppInfo('My Pregnancy Journal and LocTransie','Best Women in Science Technology Engineering and Mathematics (Stem) Solution', 'My Pregnancy Journey PTY Ltd and Ginini Consulting Apps','2019').toCaps(),
                  AppInfo('LocTransie','Best Incubated Solution','Ginini Consulting Apps', '2019').toCaps(),AppInfo('Hydra','Best Agricultural Solution', '','2019').toCaps(),AppInfo('Bottles','People’s Choice Award','Bottles App (PTY) LTD', '2019').toCaps(),
                  AppInfo('Over','Best Consumer App', 'GoDaddy Mobile, LLC','2019').toCaps(),AppInfo('Digger','Best Enterprise App','', '2019').toCaps(),AppInfo('Mo Wash','Best Breakthrough Developer', 'Runtime Solutions (Pty) Ltd','2019').toCaps(),
              'MTN App of the Year Awards Winning Apps of 2020',
                  AppInfo('EasyEquities','Best Consumer Solution', 'First World Trader (Pty) Ltd', '2020').toCaps(),AppInfo('Examsta','Best Women in STEM (science, technology, engineering, and mathematics)','', '2020').toCaps(),
                  AppInfo('Checkers Sixty60','Best Enterprise Solution', 'Shoprite Checkers (PTY) LTD','2020').toCaps(),AppInfo('Technishen','Best Incubated Solution', 'Tsogolo Technologies (PTY) Ltd','2020').toCaps(),
                  AppInfo('BirdPro','Most Innovative Solution', 'High Branching cc','2020').toCaps(),AppInfo('Lexie Hearing','Best Health Solution', 'hearX Group', '2020').toCaps(),AppInfo('League of Legends','Best Gaming Solution', '','2020').toCaps(),
                  AppInfo('GreenFingers Mobile','Best Agricultural Solution','Agro-Serve (Pty) Ltd', '2020').toCaps(),AppInfo('Xitsonga Dictionary','Best Educational Solution', 'Mukondleteri Dumela','2020').toCaps(),
                  AppInfo('StokFella','Best Financial Solution', 'Pluritone','2020').toCaps(),AppInfo('Bottles','Best ‘South African’ App', 'Bottles App (PTY) LTD','2020').toCaps(),
                  AppInfo('Matric Live','Best Breakthrough Solution', 'Lesego Finger','2020').toCaps(),AppInfo('Guardian Health','Best Youth App','Guardian Health Platform (Pty) Ltd' ,'2020').toCaps(),
                  AppInfo('My Pregnancy Journey','Huawei Category', 'My Pregnancy Journey PTY Ltd','2020').toCaps(),
              'MTN App of the Year Awards Winning Apps of 2021',
                  AppInfo('iiDENTIFii','Best Enterprise Solution', 'iiDENTIFii','2021').toCaps(),AppInfo('Hellopay SoftPOS','Most Innovative Solution', 'Transaction Technologies','2021').toCaps(),
                  AppInfo('Guardian Health Platform','Best Health Solution', 'Guardian Health Platform (Pty) Ltd','2021').toCaps(),AppInfo('Ambani Africa','Best Gaming Solution', 'Mukundi Lambani','2021').toCaps(),
                  AppInfo('Murimi','Best Agricultural Solution','Rubiem Technologies', '2021').toCaps(),AppInfo('Ambani Africa','Best Educational Solution', 'Mukundi Lambani','2021').toCaps(),
                  AppInfo('Shyft','Best Financial Solution', 'Standard Bank Group','2021').toCaps(),AppInfo('Sisa','Best Hackathon Solution', 'Claudia Mabuza','2021').toCaps(),
                  AppInfo('Ambani Africa','Best ‘South African’ App', 'Mukundi Lambani','2021').toCaps(),AppInfo('UniWise','Best Campus Cup Solution', 'OGO Studio','2021').toCaps(),
                  AppInfo('Kazi','Best African Solution','', '2021').toCaps(),AppInfo('Takealot','Best Consumer Solution', 'Takealot Online (Pty) Ltd','2021').toCaps(),
                  AppInfo('Rekindle Learning','Best Women in STEM', 'Rekindle Learning','2021').toCaps(),AppInfo('RoadSave','Huawei Category 15', 'Dynamus Technologies','2021').toCaps(),
                  AppInfo('Afrihost','People’s Choice Award', 'Afrihost (Pty) Ltd','2021').toCaps()];
          
   var overallWinningApps = ['Overall best App of the Year since 2012', AppInfo('FNB Banking','Overall best App of the Year of 2012','FNB Connect', '2012'),AppInfo('SnapScan','Overall best App of the Year of 2013','FireID (Pty) Ltd', '2013'),
                              AppInfo('LIVE Inspect','Overall best App of the Year of 2014', 'Lightstone Auto','2014'),AppInfo('WumDrop','Overall best App of the Year of 2015', 'Benjamin Claassen and Muneeb Samuels','2015'),
                              AppInfo('Domestly','Overall best App of the Year of 2016','Thatoyoana Marumo and Berno Potgieters', '2016'),AppInfo('Shyft','Overall best App of the Year of 2017','Standard Bank Group', '2017'),
                              AppInfo('Khula', 'Overall best App of the Year of 2018','Karidas Tshintsholo and Matthew Piper', '2018'),AppInfo('Naked Insurance','Overall best App of the Year of 2019','Naked Insurance', '2019'),
                              AppInfo('EasyEquities','Overall best App of the Year of 2020','First World Trader (Pty) Ltd', '2020'),AppInfo('Ambani Africa','Overall best App of the Year of 2021','Mukundi Lambani', '2021')];
  
   var overallWinningAppsCaps = ['\nOverall best App of the Year since 2012', AppInfo('FNB Banking','Overall best App of the Year of 2012','FNB Connect', '2012').toCaps(),AppInfo('SnapScan','Overall best App of the Year of 2013','FireID (Pty) Ltd', '2013').toCaps(),
                                  AppInfo('LIVE Inspect','Overall best App of the Year of 2014', 'Lightstone Auto','2014').toCaps(),AppInfo('WumDrop','Overall best App of the Year of 2015', 'Benjamin Claassen and Muneeb Samuels','2015').toCaps(),
                                  AppInfo('Domestly','Overall best App of the Year of 2016','Thatoyoana Marumo and Berno Potgieters', '2016').toCaps(),AppInfo('Shyft','Overall best App of the Year of 2017','Standard Bank Group', '2017').toCaps(),
                                  AppInfo('Khula', 'Overall best App of the Year of 2018','Karidas Tshintsholo and Matthew Piper', '2018').toCaps(),AppInfo('Naked Insurance','Overall best App of the Year of 2019','Naked Insurance', '2019').toCaps(),
                                  AppInfo('EasyEquities','Overall best App of the Year of 2020','First World Trader (Pty) Ltd', '2020').toCaps(),AppInfo('Ambani Africa','Overall best App of the Year of 2021','Mukundi Lambani', '2021').toCaps()];
                   
  
  
  for(var app in allApps){
      print(app.toString()); 
  }
  print("APPS in all CAPS:");
  for(var caps in capsApps){
      print(caps.toString());
  }

  for(var app in overallWinningApps){
      print(app.toString()); 
  }
  print("APPS in all CAPS:");
  for(var caps in overallWinningAppsCaps){
      print(caps.toString());
  }
  }

  



void main(){
  sortInfo();
}